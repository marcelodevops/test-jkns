package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.entity.Transaction;
import com.example.demo.service.TransactionService;

@Controller
public class MainController {

	@Autowired
	private TransactionService transactionService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<Transaction> main() {
		Transaction t = new Transaction();
		t.setField1("field 1");
		t.setField2("field2");
		t.setField3(getDateFormatedForFileName());
		this.transactionService.save(t);
		List<Transaction> tList = this.transactionService.findAll();
		return tList;
	}

	public static String getDateFormatedForFileName(){
		java.util.Date date = new java.util.Date();
		java.text.SimpleDateFormat sdf=new java.text.SimpleDateFormat("yyyy_MM_dd__HH_mm_ss.SSS");
		return sdf.format(date);
	}
	
}