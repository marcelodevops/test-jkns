package com.example.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Transaction;
import com.example.demo.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService{

	@Autowired
	private TransactionRepository transactionRepository;
	
	@Transactional
	public void save(Transaction t) {
		this.transactionRepository.save(t);
	}
	
	@Transactional
	public List<Transaction> findAll() {
		return this.transactionRepository.findAll();
	}
	
}
