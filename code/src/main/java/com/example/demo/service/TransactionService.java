package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Transaction;

public interface TransactionService {

	public void save(Transaction t);
	public List<Transaction> findAll();
}
